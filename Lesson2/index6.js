function maxdiv(a, b){
    var min = 0;
    var ret = 1;
    if(a > b){
        min = b;
    }
    else{
        min = a;
    }
    for(i = 1; i <= min; i++){
        if((a % i == 0) && (b % i == 0)){
            ret = i;
        }
    }
    return ret;
}
console.log(maxdiv(56, 88));