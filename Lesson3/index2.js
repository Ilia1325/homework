function TranspMatrix (matr, rows, cols){
    var nelem = rows*cols;
    var ret = new Array(nelem);
    for (var i = 0; i < rows; i++){
        for (var j = 0; j < cols; j++){
            ret[j*cols + i] = matr[i*cols + j];
        }
    }
    return ret;
}
var matr = [1, 2, 3, 4, 8, 6, 7, 15, 9, 10];
var cols = 5;
var rows = 2;
for (var i = 0; i < rows; i++){
    var rowstr = "";
    for (var j = 0; j < cols; j++){
        rowstr += " "+matr[i*cols + j];
    }
    console.log (rowstr);
}
var transpmatr = TranspMatrix (matr, rows, cols);
for (var i = 0; i < cols; i++){
    var rowstr = "";
    for (var j = 0; j < rows; j++){
        rowstr += " "+transpmatr[i*cols + j];
    }
    console.log (rowstr);
}