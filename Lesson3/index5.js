function delcol(arr, rows){
   var removes = [];
    for(var i = 0; i < arr[0].length; i++){
      var sumcol = 0;
      var zeroex = false;
      for(var j = 0; j < rows; j++){
        sumcol+= arr[j][i];
        if(arr[j][i] == 0){
            zeroex = true;
            break;
        }
      }
      if(zeroex != true && sumcol > 0){
          removes.push(i);
      }
    }
    var newData = arr.map(function(val, ind){
        return val.filter(function(val, ind) { 
            return !removes.includes(ind); 
        });
    });
    return newData;
}
console.log(delcol([[0, 1, 2], [3, 4, 5]], 2));
