function matrsum(matra, matrb, cols, rows){
    var nelem = rows*cols;
    var ret = new Array(nelem);
    for (var i = 0; i < rows; i++){
        for (var j = 0; j < cols; j++){
            ret[i*cols + j] = matra[i*cols + j] + matrb[i*cols + j];
        }
    }
    return ret;
}
var matra = [1, 2, 3, 4, 8, 6, 7, 15, 9, 10];
var matrb = [1, 2, 3, 4, 8, 6, 7, 15, 9, 10];
var cols = 5;
var rows = 2;
for (var i = 0; i < rows; i++){
    var rowstr = "";
    for (var j = 0; j < cols; j++){
        rowstr += " "+matra[i*cols + j];
    }
    console.log (rowstr);
}
for (var i = 0; i < rows; i++){
    var rowstr = "";
    for (var j = 0; j < cols; j++){
        rowstr += " "+matrb[i*cols + j];
    }
    console.log (rowstr);
}
var summatr = matrsum (matra, matrb, rows, cols);
for (var i = 0; i < rows; i++){
    var rowstr = "";
    for (var j = 0; j < cols; j++){
        rowstr += " "+summatr[i*cols + j];
    }
    console.log (rowstr);
}  
