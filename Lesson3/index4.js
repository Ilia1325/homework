function maxsumcol(arr, rows, cols){
  var max = 0;
  var col = 0;
  for(var i = 0; i < cols; i++){
    var sumcol = 0;
    for(var j = 0; j < rows; j++){
      sumcol+= arr[j][i];
    }
    if(sumcol > max){
      max = sumcol;
      col = i;
    }
  }
  return col;
}
function maxsumrow(arr, rows, cols){
  var max = 0;
  var row = 0;
  for(var i = 0; i < rows; i++){
    var sumcol = 0;
    for(var j = 0; j < cols; j++){
      sumcol+= arr[i][j];
    }
    if(sumcol > max){
      max = sumcol;
      row = i;
    }
  }
  return row;
}
console.log (maxsumcol([[0, 1, 2], [3, 4, 5]], 2, 3));
console.log (maxsumrow([[0, 1, 2], [3, 4, 5]], 2, 3));